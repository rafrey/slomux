import React from 'react'; 
import PropTypes from 'prop-types';

class Provider extends React.Component {
  // 1) Метод getChildContext(), возвращающий конкретное значение контекста
  // this.props.store - мы получаем как пропс в App 
  getChildContext() { 
    return {
      store: this.props.store,
      a: 23
    }
  } 

  // 2) Статическое свойство childContextTypes, которое описывает свойства, которые будут доступны всем потомкам.  (Будет в this.context.store) 
  static childContextTypes = {
    store: PropTypes.object,
    a: PropTypes.number,
  }

  // рендерим всех потомков
  render() {
    return this.props.children
  }
}
 

 
export default Provider
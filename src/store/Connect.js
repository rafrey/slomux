import PropTypes from 'prop-types';
import React from 'react'; 

// компоненту Child не нужно передавать контекст вниз в компонент Parent. В этом отличие от свойств, которые нужно передавать по цепочке вручную.
/**
 * Принимаем 2 параметр и возвращаем колбэк, в котоырй принимаем сам комонент(1,2)(class)
 * @param {Object} mapStateToProps состояние редакс 
 * @param {Object} mapDispatchToProps объект с методами
 */
const connect = (mapStateToProps, mapDispatchToProps) => 
Component => {
	class WrappedComponent extends React.Component {
		render() {     
			// console.warn({...mapStateToProps(this.context.store.getState(), this.props)},); 
			return (
				<Component
					{...this.props} 
					//!!! Мы получаем коллбэки которые запрашивают параметры (state\dispatch, ownProps) -> мы  ПРОСТО предаем им эти параметры -> вызываем и они делают что нужно -> и возвращаем рузльтат -объект -> отдаём как пропсы обратно
					{...mapStateToProps(this.context.store.getState(), this.props)}  
					{...mapDispatchToProps(this.context.store.dispatch, this.props)}
				/>
			)
		}

		componentDidMount() {   
			// обращаемся к глоб хранилищу и подписываем ренедр нашего компонент на получение пропсов  
			this.context.store.subscribe(this.handleChange)
		}
		// принудительный рендер
		handleChange = () => {
			this.forceUpdate()
		}
		// получаем  store  из контекста и ложим в контекст объекта this.context.store
		static contextTypes = {
			store: PropTypes.object,
			a: PropTypes.number, //пример
		}
	}
  
	return WrappedComponent
}

export default connect
import React from 'react'; 
import './App.css';
import createStore from './store/store';
import reducer from './store/counter/reducer';

import Provider from './store/Provider';

import Timer  from './components/TimerComponent';

function App() {
  return (
    <Provider store={createStore(reducer, 1)}> 
      <div className="App"> 
        <Timer></Timer>
      </div>
    </Provider>  
  );
}
 
export default App;

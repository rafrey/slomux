import connect from '../store/Connect';
import React from 'react'; 

import Interval from './IntervalComponent'; 


class TimerComponent extends React.Component {
  state = {
		currentTime: 0,
		timer: null // !!! добавил
  }

  render() {
    console.log(this.props);
    return (
      <div>
        <Interval />
        <div>
          Секундомер: {this.state.currentTime} сек.
        </div>
        <div>
          <button onClick={this.handleStart}>Старт</button>
          <button onClick={this.handleStop}>Стоп</button>
        </div>
      </div>
    )
	}
  
  componentWillReceiveProps(nextProps){   
    clearInterval(this.timer)
    this.timer = setInterval(() => this.setState({
      currentTime: this.state.currentTime + 1 ,
    }),  nextProps.currentInterval * 1000 )
  } 

	// !!! изменил   + переписал
  handleStart = () =>  {    
    console.warn(this.timer);
    if(this.timer) return
    this.timer = setInterval(() => this.setState({
      currentTime: this.state.currentTime  + 1,
    }),  this.props.currentInterval * 1000 )
  }
  
	// !!! изменил 
  handleStop = () => {
		this.setState({ currentTime: 0 })
    clearInterval(this.timer)
    this.timer = null 
  }
}

const Timer = connect(
  state => ({
  	currentInterval: state,
	}), 
	dispatch => ({ 	})
)(TimerComponent)

export default Timer

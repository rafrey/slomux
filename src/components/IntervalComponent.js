import connect from '../store/Connect';
import React from 'react'; 

import {changeInterval} from '../store/counter/action';

class IntervalComponent extends React.Component {
  render() {
		// console.log(this.props);
    return (
      <div>
        <span>Интервал обновления секундомера: {this.props.currentInterval} сек.</span>
        <span>
          <button onClick={() => this.props.changeInterval(-1)}>-</button>
          <button onClick={() => this.props.changeInterval(1)}>+</button>
        </span>
      </div>
    )
  }
}
 
const interval =  connect(
  state => ({
  	currentInterval: state,
	}), 
	(dispatch, ownProps) => ({
		changeInterval: value => dispatch(changeInterval(value)),
	})
)(IntervalComponent)

export default  interval